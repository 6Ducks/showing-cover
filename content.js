var url = document.URL;
var lastUrl = url;

setInterval(function () {
    if (lastUrl != document.URL) {
        lastUrl = document.URL;
        main();
    }
}, 500)

setTimeout(main, 0);

function change(elBackground, elImage, subjectID) {
    var imgURL = getUrl(subjectID);
    var img = document.createElement('img');
    img.src = imgURL;
    img.onload = function(){
        if( img.width > 150 ){
            console.log(imgURL);
            elBackground.style['background-image'] = 'url(' + imgURL + ')';
            elImage.style['visibility'] = 'hidden';
        }
    }
}

function $(queryselector, el) {
    if (el) {
        return el.querySelector(queryselector);
    } else {
        return document.querySelector(queryselector)
    }
}
function $$(queryselector, el) {
    if (el) {
        return el.querySelectorAll(queryselector);
    } else {
        return document.querySelectorAll(queryselector)
    }
}

function listChange() {
    var els = $$('ul.lst li');
    console.log(els);
    for (el of els) {
        try {
            change($('.photo', el), $('.photo a img', el), $('.snum', el).innerText)
        } catch(ignored){ }
    }
}

function main() {
    if (/dvd.php/.test(url)) {
        change($('.profile_gallery'), $('#dvd_img_src'), $('h1 span.tomato').innerHTML)
    } else if (/genre_av|dvd_ranking.php|dvd_list.php|mypage.php|search.php/.test(url)) {
        listChange();
    } else if (/mention/.test(url)) {
        var els = $$('ul.lst li');
        for (el of els) {
            change($('.photo', el), $('.photo a img', el), $('.k_name', el).innerText)
        }
    } 
}

function getUrl(subjectID) {
    var splitID = subjectID.split('-');
    var maker = splitID[0].toLowerCase().trim();
    var id = splitID[1].trim();

    var replaceItem = [
        {before:'kmhrs', after:'kmhr'},
        {before:'kssis', after:'ssis'},
        {before:'kcawd', after:'cawd'},
        {before:'stars', after:'star'}
    ];
    
    for( item of replaceItem){
        if(maker == item.before){
            maker = item.after;
        }
    }

    var items = [
{'regexp':/nhdtb|akdl|dandan|dandy|danuhd|dldss|fsdss|ftht|gs|hawa|hbad|hypn|ienf|iesp|kire|kmhrs|kuse|mist|msfh|mtvr|nhkb|nhvr|nyh|okb|okk|okp|oks|okx|piyo|rctd|sdab|sdam|sdde|sdfk|sdjs|sdmm|sdnm|sdth|shn|silkbt|silks|silku|stars|sun|svdvd|svomn|sw|zozo/, prefix:'1'},
{'regexp':/ekw|dfdm|ecb|dfe|wfr|wkd|wzen/, prefix:'2'},
{'regexp':/bur|lol|scr|tue/, prefix:'12'},
{'regexp':/dsvr|gvh|ovg|rvg/, prefix:'13'},
{'regexp':/mght|mond|ntrd|sprd/, prefix:'18'},
{'regexp':/dfbvr|dkd|isrd|ped|vdd/, prefix:'24'},
{'regexp':/djsk/, prefix:'29'},
{'regexp':/dksb|dkwt|doks|drop/, prefix:'36'},
{'regexp':/mdvhj|odvhj|rdvhj/, prefix:'48'},
{'regexp':/tmavr/, prefix:'55'},
{'regexp':/bdsr|husr|itsr|jksr|mcsr|sgsr/, prefix:'57'},
{'regexp':/gas/, prefix:'71'},
{'regexp':/godr|hhh/, prefix:'78'},
{'regexp':/tv|yen/, prefix:'100'},
{'regexp':/umd/, prefix:'125'},
{'regexp':/gun|neo/, prefix:'433'},
{'regexp':/bubb/, prefix:'436'},
{'regexp':/ibw/, prefix:'504'},
{'regexp':/yst/, prefix:'540'},
{'regexp':/tsds/, prefix:'5013'},

{'regexp':/id|sy|syk/, prefix:'h_113'},

// 앞문자 없는 레이블
{'regexp':/idbd/, prefix:''}, //충돌 idbd와 충돌
{'regexp':/sykh/, prefix:''}, //충돌 sky와 충돌
{'regexp':/hoks/, prefix:''}, //임의 추가

{'regexp':/hodv/, prefix:'5642'},
{'regexp':/acz|kck/, prefix:'h_019'},
{'regexp':/abba|cvdx|hone|hthd|iqqq|jrzdx|jrze|jura|mesu|nuka|toen|xmom|zeaa/, prefix:'h_086'},
{'regexp':/ktra/, prefix:'h_094'},
{'regexp':/htms/, prefix:'h_066'},
{'regexp':/jgaho/, prefix:'h_1002'},
{'regexp':/bdsm/, prefix:'h_1096'},
{'regexp':/hzgd|hzhb/, prefix:'h_1100'},
{'regexp':/nubi/, prefix:'h_1112'},
{'regexp':/cami|cabe|cafr|cafuku|capi|casmani/, prefix:'h_1116'},
{'regexp':/bstc/, prefix:'h_1117'},
{'regexp':/gopj|vosf/, prefix:'h_1127'},
{'regexp':/bbacos|kpp|spo|sw/, prefix:'h_113'},
// {'regexp':/cb/, prefix:'h_113'},  // ecb 와 충돌
{'regexp':/mhar/, prefix:'h_1160'},
{'regexp':/goju/, prefix:'h_1165'},
{'regexp':/thtp/, prefix:'h_1237'},
{'regexp':/milk/, prefix:'h_1240'},
{'regexp':/kiwvr/, prefix:'h_1248'},
{'regexp':/tpvr/, prefix:'h_1256'},
{'regexp':/ysn|ytr/, prefix:'h_127'},
{'regexp':/mtes/, prefix:'h_1300'},
{'regexp':/tg/, prefix:'h_1304'},
{'regexp':/pydvr/, prefix:'h_1321'},
{'regexp':/skmj/, prefix:'h_1324'},
{'regexp':/wvr9c|wvr6d/, prefix:'h_1337'},
{'regexp':/nsm/, prefix:'h_1342'},
{'regexp':/omt/, prefix:'h_1359'},
{'regexp':/arso/, prefix:'h_1378'},
{'regexp':/kmds/, prefix:'h_1380'},
{'regexp':/dinm/, prefix:'h_1386'},
{'regexp':/pkys/, prefix:'h_1391'},
//{'regexp':/ad/, prefix:'h_1416'}, //충돌
{'regexp':/semg|semm|sems/, prefix:'h_1422'},
{'regexp':/bth|clo/, prefix:'h_1435'},
{'regexp':/fgan/, prefix:'h_1440'},
{'regexp':/psst/, prefix:'h_1450'},
{'regexp':/com|pyu/, prefix:'h_1462'},
{'regexp':/ypp/, prefix:'h_1479'},
{'regexp':/siror/, prefix:'h_1492'},
{'regexp':/bank/, prefix:'h_1495'},
{'regexp':/pm/, prefix:'h_1526'},
{'regexp':/grmo|grmr/, prefix:'h_1534'},
{'regexp':/grkg/, prefix:'h_1535'},
{'regexp':/ftvr/, prefix:'h_1553'},
{'regexp':/shind/, prefix:'h_1560'},
{'regexp':/mol/, prefix:'h_1563'},
{'regexp':/nkkvr/, prefix:'h_1569'},
{'regexp':/ftuj/, prefix:'h_1573'},
{'regexp':/sgkx/, prefix:'h_1575'},
{'regexp':/erht|favkh|fbcpv/, prefix:'h_1577'},
//{'regexp':/ein/, prefix:'h_1587'},
{'regexp':/cubex|fanq|fanx|papak/, prefix:'h_1593'},
{'regexp':/spro/, prefix:'h_1594'},
{'regexp':/hmnf/, prefix:'h_172'},
{'regexp':/cha|milf|pc|tsm|uta|vio/, prefix:'h_189'},
{'regexp':/jukf/, prefix:'h_227'},
{'regexp':/ambi|ambs|clot|hdka|nacr|nacx|zmar/, prefix:'h_237'},
{'regexp':/dmat|doki|eih|kazk|kir|mgdn|nxg|ofku|spz|udak|vnds/, prefix:'h_254'},
{'regexp':/bgsd/, prefix:'h_305'},
{'regexp':/aoz/, prefix:'h_308'}, // 뒤에 z문자 붙음
{'regexp':/rebd/, prefix:'h_346'}, 
{'regexp':/hsm/, prefix:'h_458'},
{'regexp':/dogd/, prefix:'h_496'},
{'regexp':/tmm/, prefix:'h_580'},
{'regexp':/jbjb|macb|shm|snkh/, prefix:'h_687'},
{'regexp':/zex/, prefix:'h_720'},
{'regexp':/much|san/, prefix:'h_796'},
{'regexp':/ten/, prefix:'h_859'},
{'regexp':/nmk/, prefix:'h_897'},
{'regexp':/hj/, prefix:'h_921'}, // hjmo 와 충돌
{'regexp':/hjmo/, prefix:''}, //임의 추가
{'regexp':/lbdd/, prefix:'h_1515'},
// {'regexp':/mxsps/, prefix:'h_068'}, // 자릿수 증가 안함 "h_068mxsps666"
// {'regexp':/mxgs/, prefix:'h_068'}, // 자릿수 증가 안함, 품번뒤에 "bod"추가됨
// {'regexp':/ss/, prefix:'n_1428'}, // 자릿수 증가 안함
// {'regexp':/ovvr/, prefix:''}, // 자릿수 증가 안함
// {'regexp':/nle/, prefix:'h_1592'}, // 자릿수 증가 안함
// {'regexp':/adoa/, prefix:'n_863'}, // 자릿수 증가 안함
// {'regexp':/ghmt/, prefix:'h_173'}, // 자릿수 축소 087의 경우 87로 축소
    ]

    if (/abw|chn|abs|abp|bgn|sga|pxh/.test(maker)) {
        return `https://www.prestige-av.com/images/corner/goods/prestige/${maker}/${id}/pb_e_${maker}-${id}.jpg`
    }

    var prefix = '';
    for( item of items ){        
        if( item.regexp.test(maker) ){
            prefix = item.prefix;
        }
    }

// FANZA DMM
    return `https://pics.dmm.co.jp/digital/video/${prefix}${maker}${id.padStart(5,'0')}/${prefix}${maker}${id.padStart(5,'0')}pl.jpg`;


// R18 DMM
//    return `https://pics.r18.com/digital/video/${prefix}${maker}${id.padStart(5,'0')}/${prefix}${maker}${id.padStart(5,'0')}pl.jpg`;

}
